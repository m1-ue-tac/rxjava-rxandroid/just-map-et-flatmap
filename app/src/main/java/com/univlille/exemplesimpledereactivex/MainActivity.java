package com.univlille.exemplesimpledereactivex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.observers.DisposableObserver;

public class MainActivity extends AppCompatActivity {

    private DisposableObserver<String> disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        exempleRxJava();
    }

    private void exempleRxJava() {
        streamShowString();
    }

    // connexion à un flux et à la gestion du retour des données
    private void streamShowString() {
        this.disposable = this.getObservable()  // renvoie un Observable de type String qui contient juste la chaine 'Cool !'
                .map(getFunctionUpperCase())    // transforme l'observable précédent en majuscules
                .flatMap(getSecondObservable()) // enchaine sur le second Observable en passant le premier en paramètre automatiquement
                .subscribeWith(getSubscriber());// déclenche la connexion sur le flux et donc le onNext...
    }

    // Envoi des données sous forme de flux
    private Observable<String> getObservable() {
        return Observable.just("Cool !");
    }

    // Fonction appelée par map pour convertir en majsucules les données reçues
    private Function<String, String> getFunctionUpperCase() {
        return new Function<String, String>() {
            @Override
            public String apply(String s) throws Exception {
                return s.toUpperCase();
            }
        };
    }

    // Fonction appelée par flatmap qui :
    // - prend un Observable de type String en entrée
    // - et renvoie une String en sortie qui correspond à l'Observable en entrée + " I love that!"
    private Function<String, Observable<String>> getSecondObservable() {
        return new Function<String, Observable<String>>() {
            @Override
            public Observable<String> apply(String previousString) throws Exception {
                return Observable.just(previousString + " I love that!");
            }
        };
    }


    // Gestion des retours quand on reçoit les données
    private DisposableObserver<String> getSubscriber() {
        return new DisposableObserver<String>() {
            @Override
            public void onNext(String item) {
                TextView txtResult = findViewById(R.id.txtResult);
                txtResult.setText("Observable emits : " + item);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("TAG", "On Error" + Log.getStackTraceString(e));
            }

            @Override
            public void onComplete() {
                Log.e("TAG", "On Complete !!");
            }
        };
    }

}